module StaticPagesHelper

	def line_break(n)
		("<br/>" *n).html_safe
	end
end
